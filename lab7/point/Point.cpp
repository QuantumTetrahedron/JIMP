
#include <cmath>
#include "Point.h"

namespace geometry{

    Point::Point() {
        this->x = 0;
        this->y = 0;
    }

    Point::Point(double x, double y) : x(x), y(y) {
    }

    Point::~Point() {}

    std::ostream &operator<<(std::ostream &os, Point p) {
        os << "(" << p.x << ";" << p.y << ")";
        return os;
    }

    double Point::Distance(const Point &other) const {
        return sqrt(pow(GetX()-other.GetX(),2)+pow(GetY()-other.GetY(),2));
    }

    Point3D::Point3D() : Point() {
        this->z = 0;
    }

    Point3D::Point3D(double x, double y, double z) : Point(x,y) {
        this->z = z;
    }

    double Point3D::Distance(const Point3D &other) const {
        return sqrt(pow(GetX()-other.GetX(),2)+pow(GetY()-other.GetY(),2)+pow(GetZ()-other.GetZ(),2));
    }
}