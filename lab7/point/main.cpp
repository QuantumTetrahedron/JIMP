
#include "Point.h"

using namespace geometry;

int main(){

    Point3D p1 = Point3D(1,2,3);
    Point3D p2 = Point3D(5,5,3);
    std::cout << p1.Distance(p2) << std::endl;

    Point p = Point(0,2);
    std::cout << p.Distance(p1) << std::endl;//Punkt 3d jest castowany na punkt 2d przed wypisaniem

    std::cout << p1 << std::endl;//Jak wyżej
    return 0;
}