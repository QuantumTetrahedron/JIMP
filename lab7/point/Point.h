
#ifndef JIMP_EXERCISES_POINT_H
#define JIMP_EXERCISES_POINT_H

#include <iostream>

namespace geometry{

    class Point{
    public:
        Point();
        Point(double x, double y);

        virtual ~Point();
        double Distance(const Point& other) const;

        friend std::ostream& operator<<(std::ostream &os, Point p);

        inline double GetX() const { return x; }
        inline double GetY() const { return y; }

    protected:
        double x;
        double y;
    };
    std::ostream& operator<<(std::ostream &os, Point p);

    class Point3D : public Point{
    public:
        Point3D();
        Point3D(double x, double y, double z);

        double Distance(const Point3D& other) const;

        inline double GetZ() const { return z; }
    private:
        double z;
    };
}


#endif //JIMP_EXERCISES_POINT_H
