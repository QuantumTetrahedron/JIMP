
#include "Shapes.h"
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace geometry;

int main(){
    Ksztalt* ksztalty[20];
    srand(time(NULL));
    for(int i = 0;i<20;++i)
    {
        int r = rand()%3;
        if(r==0)
            ksztalty[i] = new Trojkat();
        else if(r==1)
            ksztalty[i] = new Kwadrat();
        else
            ksztalty[i] = new Kolo();
    }

    for(int i = 0;i<20;++i)
    {
        ksztalty[i]->Rysuj();
    }

    delete[] ksztalty;

    return 0;
}