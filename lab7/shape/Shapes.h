
#ifndef JIMP_EXERCISES_SHAPES_H
#define JIMP_EXERCISES_SHAPES_H

namespace geometry{
    class Ksztalt{
    public:
        virtual void Rysuj() = 0;
    };

    class Trojkat : public Ksztalt{
    public:
        void Rysuj() override;
    };

    class Kwadrat : public Ksztalt{
    public:
        void Rysuj() override;
    };

    class Kolo : public Ksztalt{
    public:
        void Rysuj() override;
    };
}

#endif //JIMP_EXERCISES_SHAPES_H
