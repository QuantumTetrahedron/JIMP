
#include "Shapes.h"

#include <iostream>

void geometry::Trojkat::Rysuj() {
    std::cout << "^" <<std::endl;
}

void geometry::Kwadrat::Rysuj() {
    std::cout << "[]" << std::endl;
}

void geometry::Kolo::Rysuj() {
    std::cout << "O" << std::endl;
}
