
#include "SimpleTemplateEngine.h"
#include <iostream>

using namespace nets;

int main(){
    View view{"Hello {{name}} {{asd}} {{qwe}}!"};
    const std::unordered_map<std::string, std::string> &map = {{"name", "XD"}, {"asd", "zxC"}};
    std::cout << view.Render(map) << std::endl;

    return 0;
}
