
#include "SimpleTemplateEngine.h"
#include <iostream>
namespace nets{

    View::View(std::string str) {
        pattern = str;
    }

    std::string View::Render(const std::unordered_map<std::string, std::string> &model) const {
        std::string ret = "";
        std::string tmp = "";

        bool b = false;
        for(int i=0;i<pattern.length();++i)
        {
            if(pattern[i] == '{' && pattern[i+1] == '{' && !b)
            {
                while(pattern[i+2] == '{') {
                    ret += '{';
                    ++i;
                }
                b = true;
            }

            if(pattern[i] == ' ' && b)
            {
                b = false;
                ret += "{{" + tmp + " ";
                tmp = "";
                continue;
            }

            if(pattern[i] == '}' && i<pattern.length()-1 && pattern[i+1] == '}' && b)
            {
                b = false;
                try{
                    ret += model.at(tmp);
                }
                catch(...) {}
                tmp = "";
                ++i;
                continue;
            }

            if(!b)
                ret+=pattern[i];
            else if(pattern[i]!='{')
                tmp+=pattern[i];
        }
        return ret;
    }
}