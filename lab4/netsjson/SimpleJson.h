
#ifndef JIMP_EXERCISES_SIMPLEJSON_H
#define JIMP_EXERCISES_SIMPLEJSON_H

#include <experimental/optional>
#include <string>
#include <vector>
#include <map>
#include <memory>

namespace nets{
    enum SupportedTypes{
        INT = 0,
        DOUBLE = 1,
        STRING = 2,
        BOOL = 3,
        ARRAY = 4,
        MAP = 5
    };

    class JsonValue{
    public:
        JsonValue(int n);
        JsonValue(double n);
        JsonValue(std::string str);
        JsonValue(bool b);
        JsonValue(std::vector<JsonValue> arr);
        JsonValue(std::map<std::string, JsonValue> map);
        ~JsonValue();

        std::experimental::optional<JsonValue> ValueByName(const std::string &name) const;
        std::string ToString();

    private:
        JsonValue();
        SupportedTypes type;
        std::shared_ptr<int> int_v;
        std::shared_ptr<double> dbl_v;
        std::shared_ptr<bool> bool_v;
        std::shared_ptr<std::string> str_v;
        std::shared_ptr<std::vector<JsonValue>> vec_v;
        std::shared_ptr<std::map<std::string, JsonValue>> map_v;

        std::string literal(std::string str);
    };
}

#endif //JIMP_EXERCISES_SIMPLEJSON_H
