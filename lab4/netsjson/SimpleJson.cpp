
#include <sstream>
#include "SimpleJson.h"

namespace nets{

    JsonValue::JsonValue(int n) {
        JsonValue();
        int_v = std::make_shared<int>(n);
        type = INT;
    }

    JsonValue::JsonValue(double n) {
        JsonValue();
        dbl_v = std::make_shared<double>(n);
        type = DOUBLE;
    }

    JsonValue::JsonValue(std::string str) {
        JsonValue();
        str_v = std::make_shared<std::string>(str);
        type = STRING;
    }

    JsonValue::JsonValue(bool b) {
        JsonValue();
        bool_v = std::make_shared<bool>(b);
        type = BOOL;
    }

    JsonValue::JsonValue(std::vector<JsonValue> arr) {
        JsonValue();
        vec_v = std::make_shared<std::vector<JsonValue>>(arr);
        type = ARRAY;
    }

    JsonValue::JsonValue(std::map<std::string, JsonValue> map) {
        JsonValue();
        map_v = std::make_shared<std::map<std::string, JsonValue>>(map);
        type = MAP;
    }

    std::experimental::optional<JsonValue> JsonValue::ValueByName(const std::string &name) const {
        auto ret = std::experimental::optional<JsonValue>();
        if(type == MAP)
        {
            try{
                ret = map_v->at(name);
            }
            catch(...){}
        }
        return ret;
    }

    std::string JsonValue::ToString() {
        if(type == STRING)
            return literal(*str_v);
        if(type == BOOL)
        {
            if(*bool_v)
                return "true";
            else
                return "false";
        }
        if(type == INT || type == DOUBLE)
        {
            std::stringstream ss;
            if(type == INT)
                ss << *int_v;
            else
                ss << *dbl_v;
            return ss.str();
        }
        if(type == ARRAY)
        {
            if (vec_v->size()==0)
                return "[]";
            std::string ret = "[";
            ret+=(*vec_v)[0].ToString();
            for(int i=1;i<vec_v->size();++i)
                ret+=", "+(*vec_v)[i].ToString();
            ret+="]";
            return ret;
        }
        if(type == MAP)
        {
            if(map_v->size()==0)
                return "{}";
            std::string ret = "{";
            for(auto p : *map_v){
                ret+=literal(p.first)+": "+p.second.ToString()+", ";
            }
            ret.pop_back();
            ret.pop_back();
            ret += "}";
            return ret;
        }

        return "";
    }

    JsonValue::~JsonValue() {
    }

    JsonValue::JsonValue() {
        int_v = nullptr;
        dbl_v = nullptr;
        str_v = nullptr;
        bool_v = nullptr;
        vec_v = nullptr;
        map_v = nullptr;
    }

    std::string JsonValue::literal(std::string str) {
        std::string ret = "\"";
        for(auto c : str)
        {
            if(c == '\\' || c == '\"')
                ret+='\\';
            ret += c;
        }
        ret += '\"';
        return ret;
    }
}