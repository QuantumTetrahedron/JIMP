
#ifndef JIMP_EXERCISES_TREEITERATORS_H
#define JIMP_EXERCISES_TREEITERATORS_H

#include "../tree/Tree.h"

#include <stack>
#include <queue>

namespace tree{

    template<typename T>
    class PreOrderTreeIterator{
    public:
        PreOrderTreeIterator(){}

        PreOrderTreeIterator(Tree<T>* tree){
            treeStack.push(tree);
        }
        void operator++(){
            Tree<T>* t = treeStack.top();
            treeStack.pop();
            if(t->right)
                treeStack.push(t->right.get());
            if(t->left)
                treeStack.push(t->left.get());
        }
        T operator*(){
            return treeStack.top()->Value();
        }
        bool operator!=(const PreOrderTreeIterator& other) const{
            return treeStack!=other.treeStack;
        }
    private:
        std::stack<Tree<T>*> treeStack;
    };

    template <typename T>
    class PreOrderTreeView {
    public:
        PreOrderTreeView(Tree<T>* tree):tree(tree){}
        PreOrderTreeIterator<T> begin(){
            return PreOrderTreeIterator<T>(tree);
        }
        PreOrderTreeIterator<T> end(){
            return PreOrderTreeIterator<T>();
        }
    private:
        Tree<T>* tree;
    };

    template <typename T>
    PreOrderTreeView<T> PreOrder(Tree<T>* tree){
        return PreOrderTreeView<T>(tree);
    }

    template<typename T>
    class InOrderTreeIterator{
    public:
        InOrderTreeIterator(){}

        InOrderTreeIterator(Tree<T>* tree){
            Enqueue(tree);
        }

        T operator*() const {
            return treeQ.front()->Value();
        };

        void operator++(){
            treeQ.pop();
        }

        bool operator!=(const InOrderTreeIterator& other) const{
            return treeQ!=other.treeQ;
        }

    private:
        std::queue<Tree<T>*> treeQ;
        void Enqueue(Tree<T>* t){
            if(t->left)
                Enqueue(t->left.get());
            treeQ.push(t);
            if(t->right)
                Enqueue(t->right.get());
        }
    };

    template<typename T>
    class InOrderTreeView{
    public:
        InOrderTreeView(Tree<T>* tree):tree(tree){}
        InOrderTreeIterator<T> begin(){
            return InOrderTreeIterator<T>(tree);
        }
        InOrderTreeIterator<T> end(){
            return InOrderTreeIterator<T>();
        }
    private:
        Tree<T>* tree;
    };

    template <typename T>
    InOrderTreeView<T> InOrder(Tree<T>* tree){
        return InOrderTreeView<T>(tree);
    }

    template <typename T>
    class PostOrderTreeIterator{
    public:
        PostOrderTreeIterator(){}
        PostOrderTreeIterator(Tree<T>* tree){
            Enqueue(tree);
        }

        T operator*(){
            return treeQ.front()->Value();
        }

        void operator++(){
            treeQ.pop();
        }

        bool operator!=(const PostOrderTreeIterator& other){
            return treeQ != other.treeQ;
        }

    private:
        std::queue<Tree<T>*> treeQ;
        void Enqueue(Tree<T>* t){
            if(t->left)
                Enqueue(t->left.get());
            if(t->right)
                Enqueue(t->right.get());
            treeQ.push(t);
        }
    };

    template<typename T>
    class PostOrderTreeView{
    public:
        PostOrderTreeView(Tree<T>* tree):tree(tree){}
        PostOrderTreeIterator<T> begin(){
            return PostOrderTreeIterator<T>(tree);
        }
        PostOrderTreeIterator<T> end(){
            return PostOrderTreeIterator<T>();
        }
    private:
        Tree<T>* tree;
    };

    template <typename T>
    PostOrderTreeView<T> PostOrder(Tree<T>* tree){
        return PostOrderTreeView<T>(tree);
    }

}

#endif //JIMP_EXERCISES_TREEITERATORS_H
