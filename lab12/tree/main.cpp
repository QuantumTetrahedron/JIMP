//
// Created by borebart on 30.05.17.
//

#include "Tree.h"
#include <iostream>

using namespace tree;

int main(){
    Tree<int> t(7);
    t.Insert(5);
    t.Insert(6);
    t.Insert(9);
    t.Insert(8);

    std::cout << t.Depth() << std::endl;
    std::cout << t.Size() << std::endl;
    if(!t.Find(5))
        return 1;
    return 0;
}