//
// Created by borebart on 30.05.17.
//

#ifndef JIMP_EXERCISES_TREE_H
#define JIMP_EXERCISES_TREE_H

#include <cstdlib>
#include <memory>

namespace tree{

    template<class T>
    class Tree{
    public:
        Tree(T v=T()) : value(v){}

        Tree<T>* Root(){
            return this;
        }

        void Insert(T v){
            if(v < value)
                InsertInto(left, v);
            else
                InsertInto(right, v);
        }

        bool Find(T v){
            if(v == value) return true;
            else if(v < value)
                if(left)
                    return left->Find(v);
            else
                if(right)
                    return right->Find(v);
            return false;
        }

        size_t Depth() const{
            size_t below = 0;
            if(left)
                below = left->Depth();
            if(right)
                below = std::max(below, right->Depth());
            return ++below;
        }

        size_t Size() const{
            size_t size = 1;
            if(left)
                size += left->Size();
            if(right)
                size += right->Size();
            return size;
        }

        std::unique_ptr<Tree<T>> left;
        std::unique_ptr<Tree<T>> right;

        T Value() {return value;}
    private:
        T value;

        void InsertInto(std::unique_ptr<Tree<T>>& tree, T v){
            if(tree) tree->Insert(v);
            else tree = std::make_unique<Tree<T>>(v);
        }
    };

}

#endif //JIMP_EXERCISES_TREE_H
