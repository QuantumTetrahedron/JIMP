//
// Created by borebart on 09.05.17.
//

#include "Serialization.h"
#include <functional>
namespace academia{

    Serializer::Serializer(std::ostream *out) {
        this->out = out;
    }

    Room::Room(int _id, std::string _name, Room::Type _type) {
        id = _id;
        name = _name;
        type = _type;
    }

    void Room::Serialize(Serializer *serializer) const {
        serializer->Header("room");
        serializer->IntegerField("id",id);
        serializer->StringField("name", name);
        serializer->StringField("type", TypeToString(type));
        serializer->Footer("room");
    }

    std::string Room::TypeToString(Room::Type t) const {
        switch(t) {
            case Room::Type::COMPUTER_LAB:
                return "COMPUTER_LAB";
            case Room::Type::LECTURE_HALL:
                return "LECTURE_HALL";
            case Room::Type::CLASSROOM:
                return "CLASSROOM";
        }
    }

    void XmlSerializer::IntegerField(const std::string &name, int value) {
        Header(name);
        *out << value;
        Footer(name);
    }

    void XmlSerializer::DoubleField(const std::string &name, double value) {
        Header(name);
        *out << value;
        Footer(name);
    }

    void XmlSerializer::StringField(const std::string &field_name, const std::string &value) {
        Header(field_name);
        *out << value;
        Footer(field_name);
    }

    void XmlSerializer::BooleanField(const std::string &field_name, bool value) {
        Header(field_name);
        *out << value;
        Footer(field_name);
    }

    void XmlSerializer::SerializableField(const std::string &field_name, const Serializable &value) {
        Header(field_name);
        value.Serialize(this);
        Footer(field_name);
    }

    void XmlSerializer::ArrayField(const std::string &field_name,
                                   const std::vector<std::reference_wrapper<const Serializable>> &value) {
        Header(field_name);
        for(const Serializable& v : value)
            v.Serialize(this);
        Footer(field_name);
    }

    void XmlSerializer::Header(const std::string &object_name) {
        *out << "<" << object_name << ">";
    }

    void XmlSerializer::Footer(const std::string &object_name) {
        *out << "<\\" << object_name << ">";
    }

    Building::Building(int _id, std::string _name, const std::initializer_list<Room>& _rooms)
        : id(_id), name(_name), rooms(_rooms){}

    void Building::Serialize(Serializer *serializer) const {
        const std::vector<std::reference_wrapper<const Serializable>> Rooms(rooms.begin(), rooms.end());
        serializer->Header("building");
        serializer->IntegerField("id", id);
        serializer->StringField("name",name);
        serializer->ArrayField("rooms", Rooms);
        serializer->Footer("building");
    }

    void JsonSerializer::IntegerField(const std::string &name, int value) {
        CommaIfNotFirst();
        *out << "\"" << name << "\": " << value;
    }

    void JsonSerializer::DoubleField(const std::string &name, double value) {
        CommaIfNotFirst();
        *out << "\"" << name << "\": " << value;
    }

    void JsonSerializer::StringField(const std::string &field_name, const std::string &value) {
        CommaIfNotFirst();
        *out << "\"" << field_name << "\": \"" << value << "\"";
    }

    void JsonSerializer::BooleanField(const std::string &field_name, bool value) {
        CommaIfNotFirst();
        *out << "\"" << field_name << "\": " << value;
    }

    void JsonSerializer::SerializableField(const std::string &field_name, const Serializable &value) {
        CommaIfNotFirst();
        value.Serialize(this);
    }

    void JsonSerializer::ArrayField(const std::string &field_name,
                                    const std::vector<std::reference_wrapper<const Serializable>> &value) {
        CommaIfNotFirst();
        *out << "\"" << field_name <<"\": [";
        first = true;
        for(const Serializable& v : value)
        {
            CommaIfNotFirst();
            first = true;
            v.Serialize(this);
        }
        first = false;
        *out << "]";
    }

    void JsonSerializer::Header(const std::string &object_name) {
        *out << "{";
    }

    void JsonSerializer::Footer(const std::string &object_name) {
        *out << "}";
    }

    void JsonSerializer::CommaIfNotFirst() {
        if(first)
            first = false;
        else
            *out << ", ";
    }

    void BuildingRepository::StoreAll(Serializer *serializer) {
        const std::vector<std::reference_wrapper<const Serializable>> Buildings{buildings.begin(), buildings.end()};
        serializer->Header("building_repository");
        serializer->ArrayField("buildings", Buildings);
        serializer->Footer("building_repository");
    }

    void BuildingRepository::Add(Building b) {
        buildings.push_back(b);
    }

    std::experimental::optional<Building> BuildingRepository::operator[](int b_id) const {
        auto ret = std::experimental::optional<Building>();
        for(const Building& b : buildings)
        {
            if(b.Id() == b_id)
            {
                ret = b;
                break;
            }
        }
        return ret;
    }
}