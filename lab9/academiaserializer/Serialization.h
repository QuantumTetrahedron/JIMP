//
// Created by borebart on 09.05.17.
//

#ifndef JIMP_EXERCISES_SERIALIZATION_H
#define JIMP_EXERCISES_SERIALIZATION_H

#include <string>
#include <iostream>
#include <vector>
#include <experimental/optional>


namespace academia{
    class Serializable;
    class Serializer{
    public:
        Serializer(std::ostream *out);
        virtual void IntegerField(const std::string& name, int value) = 0;
        virtual void DoubleField(const std::string& name, double value) = 0;
        virtual void StringField(const std::string &field_name, const std::string &value) = 0;
        virtual void BooleanField(const std::string &field_name, bool value) = 0;
        virtual void SerializableField(const std::string &field_name, const Serializable &value) = 0;
        virtual void ArrayField(const std::string &field_name, const std::vector<std::reference_wrapper<const Serializable>> &value) = 0;
        virtual void Header(const std::string &object_name) = 0;
        virtual void Footer(const std::string &object_name) = 0;
        std::ostream* out;
    };

    class XmlSerializer : public Serializer{
    public:

        XmlSerializer(std::ostream *out) : Serializer(out){}
        void IntegerField(const std::string &name, int value) override;

        void DoubleField(const std::string &name, double value) override;

        void StringField(const std::string &field_name, const std::string &value) override;

        void BooleanField(const std::string &field_name, bool value) override;

        void SerializableField(const std::string &field_name, const Serializable &value) override;

        void ArrayField(const std::string &field_name,
                        const std::vector<std::reference_wrapper<const Serializable>> &value) override;

        void Header(const std::string &object_name) override;

        void Footer(const std::string &object_name) override;

    };

    class JsonSerializer : public Serializer{
    public:
        JsonSerializer(std::ostream *out) : Serializer(out), first(true){}
        void IntegerField(const std::string &name, int value) override;

        void DoubleField(const std::string &name, double value) override;

        void StringField(const std::string &field_name, const std::string &value) override;

        void BooleanField(const std::string &field_name, bool value) override;

        void SerializableField(const std::string &field_name, const Serializable &value) override;

        void ArrayField(const std::string &field_name,
                        const std::vector<std::reference_wrapper<const Serializable>> &value) override;

        void Header(const std::string &object_name) override;

        void Footer(const std::string &object_name) override;
    private:
        bool first;
        void CommaIfNotFirst();
    };

    class Serializable{
    public:
        virtual void Serialize(Serializer *serializer) const = 0;
    };

    class Room : public Serializable{
    public:
        enum class Type{
            COMPUTER_LAB,
            LECTURE_HALL,
            CLASSROOM
        };

        Room(int _id, std::string _name, Type _type);
        void Serialize(Serializer *serializer) const override;
    private:
        std::string TypeToString(Type t) const;

        int id;
        std::string name;
        Type type;
    };

    class Building : public Serializable {
    public:
        Building(int _id, std::string _name, const std::initializer_list<Room>& _rooms);
        void Serialize(Serializer *serializer) const override;
        inline int Id() const { return id; }

    private:
        int id;
        std::string name;
        std::vector<Room> rooms;
    };

    class BuildingRepository{
    public:
        BuildingRepository(const std::initializer_list<Building>& _buildings)
                : buildings(_buildings) {}

        void StoreAll(Serializer *serializer);
        void Add(Building b);
        std::vector<Building> buildings;
        std::experimental::optional<Building> operator[](int b_id) const;
    };

}


#endif //JIMP_EXERCISES_SERIALIZATION_H
