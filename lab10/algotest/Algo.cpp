//
// Created by borebart on 16.05.17.
//

#include "Algo.h"
#include <algorithm>
#include <set>
#include <map>

namespace algo{

    void CopyInto(const std::vector<int> &v, int n_elements, std::vector<int> *out) {
        std::copy_n(v.begin(), n_elements, std::back_inserter(*out));
    }

    std::set<std::string> Keys(const std::map<std::string, int> &m) {
        std::set<std::string> ret;
        std::transform(std::begin(m), std::end(m), std::inserter(ret, ret.begin()), [](std::pair<std::string, int> p){ return p.first;});
        return ret;
    }

    std::vector<int> Values(const std::map<std::string, int> &m) {
        std::vector<int> ret;
        std::transform(std::begin(m), std::end(m), std::back_inserter(ret), [](std::pair<std::string, int> p){return p.second;});
        return ret;
    }

    std::map<std::string, int> DivisableBy(const std::map<std::string, int> &m, int divisor) {
        std::map<std::string, int> ret;
        std::copy_if(std::begin(m), std::end(m), std::inserter(ret, std::begin(ret)), [divisor](std::pair<std::string, int> p){return p.second % divisor == 0;});
        return ret;
    }

    void SortInPlace(std::vector<int> *v) {
        std::sort(std::begin(*v), std::end(*v));
    }

    std::vector<int> Sort(const std::vector<int> &v) {
        std::vector<int> ret;
        std::copy(std::begin(v), std::end(v), std::back_inserter(ret));
        std::sort(std::begin(ret), std::end(ret));
        return ret;
    }

    void SortByFirstInPlace(std::vector<std::pair<int, int>> *v) {
        std::sort(std::begin(*v), std::end(*v), [](std::pair<int, int> p1, std::pair<int, int> p2){return p1.first < p2.first;});
    }

    void SortBySecondInPlace(std::vector<std::pair<int, int>> *v) {
        std::sort(std::begin(*v), std::end(*v), [](std::pair<int, int> p1, std::pair<int, int> p2){return p1.second < p2.second;});
    }

    void SortByThirdInPlace(std::vector<std::tuple<int, int, int>> *v) {
        std::sort(std::begin(*v), std::end(*v), [](std::tuple<int, int, int> p1, std::tuple<int, int, int> p2){return std::get<2>(p1) < std::get<2>(p2);});
    }

    std::vector<std::string> MapToString(const std::vector<double> &v) {
        std::vector<std::string> ret(v.size());
        std::transform(v.begin(), v.end(), std::back_inserter(ret), [](double d){return std::to_string(d);});
        return ret;
    }

    std::string Join(const std::string &joiner, const std::vector<double> &v) {
        return std::__cxx11::string();
    }

    int Sum(const std::vector<int> &v) {
        int n=0;
        std::for_each(v.begin(), v.end(), [&n](int x){ n+=x; });
        return n;
    }

    int Product(const std::vector<int> &v) {
        int n=1;
        std::for_each(v.begin(), v.end(), [&n](int x){ n*=x; });
        return n;
    }

    bool Contains(const std::vector<int> &v, int element) {
        return std::find(std::begin(v), std::end(v), element) != std::end(v);
    }

    bool ContainsKey(const std::map<std::string, int> &v, const std::string &key) {
        return v.find(key) != std::end(v);
    }

    bool ContainsValue(const std::map<std::string, int> &v, int value) {
        return std::find_if(std::begin(v), std::end(v), [value](std::pair<std::string, int> p){return p.second == value;}) != std::end(v);
    }

    std::vector<std::string> RemoveDuplicates(const std::vector<std::string> &v) {
        std::vector<std::string> ret;
        std::copy(v.begin(), v.end(), std::back_inserter(ret));
        auto last = std::unique(std::begin(ret), std::end(ret));
        ret.erase(last, ret.end());
        return ret;
    }

    void RemoveDuplicatesInPlace(std::vector<std::string> *v) {
        auto last = std::unique(std::begin(*v), std::end(*v));
        v->erase(last, std::end(*v));
    }

    void InitializeWith(int initial_value, std::vector<int> *v) {
        std::fill(std::begin(*v), std::end(*v), initial_value);
    }

    std::vector<int> InitializedVectorOfLength(int length, int initial_value) {
        std::vector<int> ret(length);
        InitializeWith(initial_value, &ret);
        return ret;
    }

    int HowManyShortStrings(const std::vector<std::string> &v, int inclusive_short_length) {
        int n=0;
        std::for_each(v.begin(), v.end(), [&n](std::string str){ if(str.length()<=inclusive_short_length)n++; });
        return n;
    }
}