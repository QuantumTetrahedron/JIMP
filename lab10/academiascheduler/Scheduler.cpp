
#include "Scheduler.h"
#include <algorithm>
#include <map>

namespace academia{

    SchedulingItem::SchedulingItem(int course, int teacher, int room, int time, int y)
        : course_id(course), teacher_id(teacher), room_id(room), time_slot(time), year(y){}

    Schedule Schedule::OfTeacher(int teacher_id) const {
        Schedule ret = Schedule();
        std::copy_if(std::begin(items), std::end(items), std::back_inserter(ret.items), [teacher_id](const SchedulingItem& item){ return item.TeacherId() == teacher_id; });
        return ret;
    }

    Schedule Schedule::OfRoom(int room_id) const {
        Schedule ret = Schedule();
        std::copy_if(std::begin(items), std::end(items), std::back_inserter(ret.items), [room_id](const SchedulingItem& item){ return item.RoomId() == room_id; });
        return ret;
    }

    Schedule Schedule::OfYear(int year) const {
        Schedule ret = Schedule();
        std::copy_if(std::begin(items), std::end(items), std::back_inserter(ret.items), [year](const SchedulingItem& item){ return item.Year() == year; });
        return ret;
    }

    std::vector<int> Schedule::AvailableTimeSlots(int n_time_slots) const {
        std::vector<int> t(n_time_slots);
        int n=0;
        std::generate(std::begin(t), std::end(t), [&n]{return ++n;});
        std::vector<SchedulingItem> tmp(items);
        std::vector<int> ret;
        std::copy_if(std::begin(t), std::end(t), std::back_inserter(ret), [tmp](int time_slot){
            return std::find_if(std::begin(tmp), std::end(tmp), [time_slot](const SchedulingItem& item){
                return item.TimeSlot() == time_slot;}) == std::end(tmp);
        });

        return ret;
    }

    void Schedule::InsertScheduleItem(const SchedulingItem &item) {
        items.emplace_back(item);
    }

    size_t Schedule::Size() const {
        return items.size();
    }

    SchedulingItem Schedule::operator[](int n) const {
        return items[n];
    }

    Schedule GreedyScheduler::PrepareNewSchedule(const std::vector<int> &rooms,
                                                 const std::map<int, std::vector<int>> &teacher_courses_assignment,
                                                 const std::map<int, std::set<int>> &courses_of_year,
                                                 int n_time_slots) {
        Schedule ret;

        for(const std::pair<const int, std::vector<int>>& p : teacher_courses_assignment)
        {
            int tch = p.first;
            for(int course : p.second)
            {
                bool roomFound = false;
                for(int room :rooms)
                {
                    for(int time : ret.OfTeacher(tch).AvailableTimeSlots(n_time_slots))
                    {
                        std::vector<int> roomTimes = ret.OfRoom(room).AvailableTimeSlots(n_time_slots);
                        if(std::find(std::begin(roomTimes), std::end(roomTimes), time) != std::end(roomTimes))
                        {
                            int y = -1;
                            for(std::pair<int, std::set<int>> years : courses_of_year)
                            {
                                std::vector<int> yearTimes = ret.OfYear(years.first).AvailableTimeSlots(n_time_slots);
                                if(std::find(std::begin(yearTimes), std::end(yearTimes), time) != std::end(yearTimes)
                                   && std::find(years.second.begin(), years.second.end(), course) != years.second.end())
                                {
                                    y = years.first;
                                    break;
                                }
                            }
                            if(y!=-1)
                            {
                                ret.InsertScheduleItem(SchedulingItem(course, tch, room, time, y));
                                roomFound = true;
                                break;
                            }
                            else
                                throw NoViableSolutionFound();
                        }
                    }
                    if(roomFound)
                        break;
                }
                if(!roomFound)
                    throw NoViableSolutionFound();
            }
        }
        return ret;
    }
}