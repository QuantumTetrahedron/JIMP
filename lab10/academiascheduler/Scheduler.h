
#ifndef JIMP_EXERCISES_SCHEDULER_H
#define JIMP_EXERCISES_SCHEDULER_H

#include <vector>
#include <set>
#include <map>
#include <cstdlib>

namespace academia{
    class SchedulingItem{
    public:
        SchedulingItem(int course, int teacher, int room, int time, int y);
        inline int CourseId() const {return course_id;}
        inline int TeacherId() const { return teacher_id; }
        inline int RoomId() const {return room_id;}
        inline int TimeSlot() const { return time_slot;}
        inline int Year() const { return year;}

    private:
        int course_id;
        int teacher_id;
        int room_id;

        int time_slot;
        int year;
    };

    class Schedule{
    public:
        Schedule OfTeacher(int teacher_id) const;
        Schedule OfRoom(int room_id) const;
        Schedule OfYear(int year) const;
        std::vector<int> AvailableTimeSlots(int n_time_slots) const;
        void InsertScheduleItem(const SchedulingItem& item);
        size_t Size() const;
        SchedulingItem operator[](int n) const;
    private:
        std::vector<SchedulingItem> items;
    };

    class Scheduler{
    public:
        virtual Schedule PrepareNewSchedule(const std::vector<int> &rooms, const std::map<int, std::vector<int>> &teacher_courses_assignment, const std::map<int, std::set<int>> &courses_of_year, int n_time_slots) = 0;
    };

    class NoViableSolutionFound : public std::runtime_error{
    public:
        NoViableSolutionFound() : std::runtime_error("no viable solution"){}
    };

    class GreedyScheduler : public Scheduler{
    public:
        Schedule PrepareNewSchedule(const std::vector<int> &rooms, const std::map<int, std::vector<int>> &teacher_courses_assignment, const std::map<int, std::set<int>> &courses_of_year, int n_time_slots) override;
    };

}

#endif //JIMP_EXERCISES_SCHEDULER_H
