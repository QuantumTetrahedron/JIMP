//
// Created by borebart on 28.02.17.
//
#include <iostream>
#include "MultiplicationTable.h"

int main() {
    int tab[10][10];
    MultiplicationTable(tab);
    std::cout << tab[3][4] << std::endl;
    return 0;
}

