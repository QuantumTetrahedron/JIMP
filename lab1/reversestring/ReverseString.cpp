//
// Created by borebart on 28.02.17.
//

#include "ReverseString.h"

unsigned int LastIndex(const std::string &str);

std::string Reverse(std::string str)
{
    if(str.length() < 2)
        return str;
    return str[LastIndex(str)] + Reverse(str.substr(0, LastIndex(str)));
}

unsigned int LastIndex(const std::string &str) { return str.length() - 1; }