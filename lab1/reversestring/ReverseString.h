//
// Created by borebart on 28.02.17.
//

#ifndef JIMP_EXERCISES_REVERSESTRING_H
#define JIMP_EXERCISES_REVERSESTRING_H

#include <string>

std::string Reverse(std::string str);

#endif //JIMP_EXERCISES_REVERSESTRING_H
