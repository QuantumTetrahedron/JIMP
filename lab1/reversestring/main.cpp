//
// Created by borebart on 28.02.17.
//
#include <iostream>
#include "ReverseString.h"

int main() {
    std::cout << "qwerty -- " << Reverse("qwerty") << std::endl;
    return 0;
}
