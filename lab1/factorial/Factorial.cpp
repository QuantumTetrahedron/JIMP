//
// Created by mwypych on 02.02.17.
//
#include "Factorial.h"

int Factorial(int value) {
    if(value > 12 || value < -12)
       return 0;

    int ret = 1;
    for(int i=value;i!=0;i=value<0?i+1:i-1)
    {
        ret*=i;
    }
    return ret;
}