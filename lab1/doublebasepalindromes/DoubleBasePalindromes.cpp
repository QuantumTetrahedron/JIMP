#include <vector>
#include "DoubleBasePalindromes.h"

bool isPalindrome(int num, int base);

uint64_t DoubleBasePalindromes(int max_value_exclusive)
{
    uint64_t ret = 0;

    for(int i=1;i<max_value_exclusive;i+=2)
    {
        if(isPalindrome(i,10) && isPalindrome(i,2))
            ret += i;
    }

    return ret;
}

bool isPalindrome(int num, int base)
{
    std::vector<int> vec;
    while(num > 0)
    {
        vec.push_back(num%base);
        num/=base;
    }

    for(int i=0;i<vec.size();++i)
    {
        if(vec[i] != vec[vec.size()-i-1])
            return false;
    }
    return true;
}