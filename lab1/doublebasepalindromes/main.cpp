#include <iostream>
#include "DoubleBasePalindromes.h"

int main()
{
    std::cout << "Sum of double base palindromes up to 1.000.000 = " << DoubleBasePalindromes(1000000) << std::endl;

    return 0;
}
