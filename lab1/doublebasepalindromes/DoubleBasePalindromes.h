
#ifndef JIMP_EXERCISES_DOUBLEBASEPALINDROMES_H
#define JIMP_EXERCISES_DOUBLEBASEPALINDROMES_H

#include <cstdint>

uint64_t DoubleBasePalindromes(int max_value_exclusive);

#endif //JIMP_EXERCISES_DOUBLEBASEPALINDROMES_H
