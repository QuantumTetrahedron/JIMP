#include <iostream>
#include "Palindrome.h"

int main(){
    char c;
    std::string str = "";
    do{
        std::cout << "1) Sprawdz palindrom" << std::endl;
        std::cout << "2) Wyjscie" << std::endl;

        std::string in;
        std::cin >> in;
        if(in.length() != 1){
            std::cout << "nierozpoznana opcja" << std::endl;
            continue;
        }
        c = in[0];

        if(c=='2')
            break;
        std::cin >> str;

        if(str.length()!=0) {
            if (IsPalindrome(str))
                std::cout << str << " jest palindromem" << std::endl;
            else
                std::cout << str << " nie jest palindromem" << std::endl;
        }
    }while(true);

    return 0;
}
