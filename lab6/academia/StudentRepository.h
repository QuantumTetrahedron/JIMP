//
// Created by borebart on 04.04.17.
//

#ifndef JIMP_EXERCISES_STUDENTREPOSITORY_H
#define JIMP_EXERCISES_STUDENTREPOSITORY_H

#include <string>
#include <vector>
#include <initializer_list>

namespace academia{
    class StudyYear{
    public:
        StudyYear();
        StudyYear(int v);
        StudyYear &operator++();
        StudyYear &operator--();
        bool operator<(const StudyYear& other) const;
        bool operator==(const StudyYear& other) const;
        operator int() const;
        int year;
    };

    class Student{
    public:
        Student();
        Student(std::string _id, std::string first, std::string last, std::string prog, int y);
        inline std::string Id() const { return id; }
        inline std::string FirstName() const { return first_name; }
        inline std::string LastName() const { return last_name; }
        inline std::string Program() const { return program; }
        inline StudyYear Year() const { return year; }
        inline void ChangeLastName(std::string n) { last_name = n; }

        bool operator==(const Student& other) const;

    private:
        std::string id;
        std::string first_name;
        std::string last_name;
        std::string program;
        StudyYear year;
    };

    class StudentRepository{
    public:
        StudentRepository();
        StudentRepository(std::initializer_list<Student> list);

        bool operator==(const StudentRepository& other);
        Student &operator[](std::string _id);

        std::vector<Student> students;
    };

    std::ostream& operator<<(std::ostream &os, StudyYear year);
    std::istream& operator>>(std::istream &is, StudyYear& year);

    std::ostream& operator<<(std::ostream &os, Student s);
    std::istream& operator>>(std::istream &is, Student& s);

    std::ostream& operator<<(std::ostream &os, StudentRepository rep);
    std::istream& operator>>(std::istream &is, StudentRepository& rep);
}


#endif //JIMP_EXERCISES_STUDENTREPOSITORY_H
