//
// Created by borebart on 04.04.17.
//

#include "StudentRepository.h"
#include <iostream>

using namespace academia;

int main(){
    StudyYear y(5);

    std::cin >> y;
    StudyYear y2 = ++y;

    std::cout << y2 << std::endl;

    StudentRepository rep{{"a","b","c","d",5}, {"q","w","e","r",7}};
    std::cout << rep << std::endl;
    std::cout << rep["a"] << std::endl;
    return 0;
}