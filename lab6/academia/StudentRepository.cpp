//
// Created by borebart on 04.04.17.
//

#include "StudentRepository.h"

#include <iostream>

namespace academia{

    StudyYear &StudyYear::operator++() {
        ++year;
        return *this;
    }


    StudyYear &StudyYear::operator--() {
        --year;
        return *this;
    }

    StudyYear::StudyYear() {
        year = 1;
    }

    StudyYear::StudyYear(int v) {
        year = v;
    }

    bool StudyYear::operator<(const StudyYear &other) const {
        return year<other.year;
    }

    bool StudyYear::operator==(const StudyYear &other) const {
        return year==other.year;
    }

    StudyYear::operator int() const {
        return year;
    }

    std::ostream& operator<<(std::ostream &os, StudyYear year)
    {
        os << year.year;
        return os;
    }

    std::istream& operator>>(std::istream &is, StudyYear& year)
    {
        is >> year.year;
        return is;
    }

    std::ostream& operator<<(std::ostream &os, Student s){
        os << "{id: \""+s.Id()+"\", first_name: \""+s.FirstName()+"\", last_name: \""+s.LastName()+"\", program: \""+s.Program()+"\", year: ";
        os << s.Year();
        os << "}";
        return os;
    }

    std::istream& operator>>(std::istream &is, Student& s){
        return is;
    }

    std::ostream& operator<<(std::ostream &os, StudentRepository rep){
        os << "[";
        for(int i=0;i<rep.students.size();++i) {
            os << rep.students[i];
            if(i!=rep.students.size()-1)
                os << ", ";
        }
        os << "]";
        return os;
    }

    std::istream& operator>>(std::istream &is, StudentRepository& rep){
        return is;
    }

    Student::Student() {
        id="";
        first_name="";
        last_name="";
        program="";
        year=StudyYear();
    }

    Student::Student(std::string _id, std::string first, std::string last, std::string prog, int y) {
        id=_id;
        first_name=first;
        last_name=last;
        program=prog;
        year=StudyYear(y);
    }

    bool Student::operator==(const Student &other) const {
        return id==other.Id()&&first_name==other.FirstName()&&last_name==other.LastName()&&program==other.Program()&&year==other.Year();
    }

    StudentRepository::StudentRepository() {
    }

    StudentRepository::StudentRepository(std::initializer_list<Student> list) : students(list){
    }

    bool StudentRepository::operator==(const StudentRepository &other) {
        if(students.size() != other.students.size())
            return false;
        for(int i=0;i<students.size();++i)
        {
            if(!(students[i] == other.students[i]))
                return false;
        }
        return true;
    }

    Student &StudentRepository::operator[](std::string _id) {
        for(auto& s : students)
        {
            if(s.Id() == _id)
                return s;
        }
        students.emplace_back(_id,"","","",1);

    }
}

