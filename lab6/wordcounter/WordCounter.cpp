
#include "WordCounter.h"

#include <fstream>
#include <iostream>
#include <list>

namespace datastructures{

    WordCounter::WordCounter(std::string path) {
        std::fstream in(path, std::ios_base::in | std::ios_base::out | std::ios_base::app);
        Word w;
        while(!in.eof())
        {
            in >> w;
            w = w.norm();
            if(w.len() > 0)
                AddWord(w);
        }
    }

    bool WordCounter::HasWord(Word w) {
        try{
            counter.at(w);
            return true;
        }
        catch(...){
            return false;
        }
    }

    void WordCounter::AddWord(Word w) {
        if(HasWord(w))
            ++counter[w];
        else
            counter[w] = 1;
    }

    Counts WordCounter::operator[](std::string str) {
        if(HasWord(str))
            return counter[str];
        else
            return Counts(0);
    }

    Word::operator std::string() {
        return this->word;
    }

    Word &Word::operator=(std::string str) {
        this->word = str;
        return *this;
    }

    std::string Word::norm() const {
        std::string ret ="";
        for(char c : word)
        {
            if((c >= 'a' && c<='z') || (c>='A' && c <='Z'))
                ret+=c | 32;
        }
        return ret;
    }

    unsigned long Word::len() const {
        return word.length();
    }

    Word &Word::operator=(const char *str) {
        this->word = str;
        return *this;
    }

    Word::Word(std::string &str) {
        this->word=str;
    }

    Word::Word() {
        this->word = "";
    }

    Word::Word(const char *str) {
        this->word = str;
    }

    bool Word::operator<(const Word w) const {
        return this->word < w.GetWord();
    }

    bool Word::operator==(const Word w) const {
        return this->word == w.GetWord();
    }

    bool Word::operator>(const Word w) const {
        return this->word > w.GetWord();
    }

    std::string Word::GetWord() const {
        return this->word;
    }

    Counts &Counts::operator=(int value) {
        this->c = value;
        return *this;
    }

    Counts::operator int() const {
        return this->c;
    }

    Counts::Counts(int v) {
        c = v;
    }

    Counts::Counts() {
        c = 0;
    }

    Counts &Counts::operator++() {
        ++this->c;
        return *this;
    }

    bool Counts::operator<(const Counts other) const {
        return this->c<(int)other;
    }

    bool Counts::operator==(const Counts other) const {
        return this->c==(int)other;
    }

    bool Counts::operator>(const Counts other) const {
        return this->c>(int)other;
    }

    std::istream &operator>>(std::istream &in, Word& w) {
        std::string tmp;
        in >> tmp;
        w = tmp;
        return in;
    }

    bool Compare(const std::pair<Word, Counts> &first, const std::pair<Word, Counts> &second){
        return first.second > second.second;
    }

    std::ostream &operator<<(std::ostream &out, WordCounter counter) {
        out << "Total words:\t" << counter.TotalWords() << std::endl;
        out << "Distinct words:\t" << counter.DistinctWords() << std::endl;
        std::list<std::pair<Word,Counts>> words;
        for(std::pair<Word, Counts> pair : counter.counter) {
            words.push_back(pair);
        }
        words.sort(Compare);
        for(auto word : words){
            out << (int)word.second << ":  " << (std::string)word.first <<std::endl;
        }
        return out;
    }

    WordCounter::WordCounter() {
    }

    WordCounter::WordCounter(std::initializer_list<Word> list) {
        for(Word w : list)
            AddWord(w);
    }

    std::set<Word> WordCounter::Words() {
        std::set<Word> ret;
        for(auto p : counter)
            ret.emplace(p.first);
        return ret;
    }

    int WordCounter::DistinctWords() {
        return counter.size();
    }

    int WordCounter::TotalWords() {
        int ret = 0;
        for(auto p : counter)
            ret += p.second;
        return ret;
    }
}