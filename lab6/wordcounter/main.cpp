#include "WordCounter.h"
#include <iostream>

using namespace datastructures;

int main(){
    WordCounter wc("file.txt");
    std::cout << wc << std::endl;
    return 0;
}