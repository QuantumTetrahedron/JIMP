
#ifndef JIMP_EXERCISES_WORDCOUNTER_H
#define JIMP_EXERCISES_WORDCOUNTER_H

#include <string>
#include <map>
#include <istream>
#include <ostream>
#include <initializer_list>
#include <set>

namespace datastructures{

    class Word{
    public:
        Word();
        Word(std::string &str);
        Word(const char* str);
        operator std::string();
        Word &operator=(std::string str);
        Word &operator=(const char* str);
        bool operator<(const Word w) const;
        bool operator==(const Word w) const;
        bool operator>(const Word w) const;
        std::string norm() const;
        unsigned long len() const;
        std::string GetWord() const;
    private:
        std::string word;
    };

    class Counts{
    public:
        Counts();
        Counts(int v);
        Counts &operator=(int value);
        Counts &operator++();
        bool operator<(const Counts other) const;
        bool operator==(const Counts other) const;
        bool operator>(const Counts other) const;
        operator int() const;
    private:
        int c;
    };

    class WordCounter{
    public:
        WordCounter();
        WordCounter(std::initializer_list<Word> list);
        WordCounter(std::string path);
        Counts operator[](std::string str);
        std::set<Word> Words();
        int DistinctWords();
        int TotalWords();

        friend std::ostream &operator<<(std::ostream &out, WordCounter counter);
    private:
        bool HasWord(Word w);
        void AddWord(Word w);

        std::map<Word, Counts> counter;
    };

    bool Compare(const std::pair<Word, Counts> &first, const std::pair<Word, Counts> &second);
    std::istream &operator>>(std::istream &in, Word& w);
    std::ostream &operator<<(std::ostream &out, WordCounter counter);
}

#endif //JIMP_EXERCISES_WORDCOUNTER_H
