//
// Created by borebart on 07.03.17.
//
#include <iostream>
#include "Array2D.h"


int** Array2D(int n_rows, int n_columns){
    if(n_rows <=0 || n_columns <=0)
        return nullptr;

    int **ret = NewArray2D(n_rows, n_columns);
    FillArray2D(n_rows, n_columns, ret);

    return ret;
}

int **NewArray2D(int n_rows, int n_columns) {
    int** ret = new int*[n_rows];
    for (int i=0;i<n_rows;++i){
        ret[i] = new int[n_columns];
    }
    return ret;
}


void DeleteArray2D(int** array, int n_rows, int n_columns){
    for(int i=0;i<n_rows;++i){
        delete(array[i]);
    }
    delete array;
}


void FillArray2D(int n_rows, int n_columns, int **array) {
    for(int i=0;i<n_rows;++i){
        for(int j=0;j<n_columns;++j){
            array[i][j] = j+1+i*n_columns;
        }
    }
}

void PrintArray2D(int **array, int n_rows, int n_columns){
    for(int i=0;i<n_rows;++i){
        for(int j=0;j<n_columns;++j){
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }
}