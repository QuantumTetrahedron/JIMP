//
// Created by borebart on 07.03.17.
//

#ifndef JIMP_EXERCISES_ARRAY2D_H
#define JIMP_EXERCISES_ARRAY2D_H

int **NewArray2D(int n_rows, int n_columns);
int** Array2D(int n_rows, int n_columns);
void DeleteArray2D(int** array, int n_rows, int n_columns);

void FillArray2D(int n_rows, int n_columns, int **array);
void PrintArray2D(int **array, int n_rows, int n_columns);

#endif //JIMP_EXERCISES_ARRAY2D_H
