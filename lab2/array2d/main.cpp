//
// Created by borebart on 07.03.17.
//
#include "Array2D.h"
#include <iostream>

int main()
{
    int a, b;
    std::cin >> a;
    std::cin >> b;

    int** arr = Array2D(a, b);

    PrintArray2D(arr, a, b);

    DeleteArray2D(arr, a, b);

    return 0;
}
