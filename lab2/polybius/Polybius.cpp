
#include "Polybius.h"
#include <iostream>

std::vector<std::vector<std::string>> GetPairs(){
    std::vector<std::vector<std::string>> ret;
    ret.push_back({"11", "A", "a"});
    ret.push_back({"12", "B", "b"});
    ret.push_back({"13", "C", "c"});
    ret.push_back({"14", "D", "d"});
    ret.push_back({"15", "E", "e"});
    ret.push_back({"21", "F", "f"});
    ret.push_back({"22", "G", "g"});
    ret.push_back({"23", "H", "h"});
    ret.push_back({"24", "I", "i", "J", "j"});
    ret.push_back({"25", "K", "k"});
    ret.push_back({"31", "L", "l"});
    ret.push_back({"32", "M", "m"});
    ret.push_back({"33", "N", "n"});
    ret.push_back({"34", "O", "o"});
    ret.push_back({"35", "P", "p"});
    ret.push_back({"41", "Q", "q"});
    ret.push_back({"42", "R", "r"});
    ret.push_back({"43", "S", "s"});
    ret.push_back({"44", "T", "t"});
    ret.push_back({"45", "U", "u"});
    ret.push_back({"51", "V", "v"});
    ret.push_back({"52", "W", "w"});
    ret.push_back({"53", "X", "x"});
    ret.push_back({"54", "Y", "y"});
    ret.push_back({"55", "Z", "z"});
    return ret;
}

std::string PolybiusCrypt(std::string message){
    auto pairs = GetPairs();
    std::string ret = "";
    bool b;
    for(char ch : message) {
        b = false;
        for (auto p : pairs) {
            for(int i=1;i<p.size();++i){
                if(p[i][0] == ch)
                {
                    ret += p[0];
                    b = true;
                    break;
                }
            }
            if(b) break;
        }
    }
    return ret;
}

std::string PolybiusDecrypt(std::string crypted){
    std::vector<char> in;
    std::string ret = "";
    auto pairs = GetPairs();
    for(char ch : crypted)
    {
        if(ch != ' ')
        {
            in.push_back(ch);
        }

        if(in.size() == 2) {
            std::string tmp = "";
            tmp += in[0];
            tmp += in[1];
            for(auto p : pairs) {
                if (tmp == p[0]) {
                    ret += p[2];
                    break;
                }
            }
            in.clear();
        }
    }
    return ret;
}