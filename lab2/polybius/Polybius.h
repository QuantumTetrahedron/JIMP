
#ifndef JIMP_EXERCISES_POLYBIUS_H
#define JIMP_EXERCISES_POLYBIUS_H

#include <string>
#include <vector>

std::vector<std::vector<std::string>> GetPairs();
std::string PolybiusCrypt(std::string message);
std::string PolybiusDecrypt(std::string crypted);

#endif //JIMP_EXERCISES_POLYBIUS_H
