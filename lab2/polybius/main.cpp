
#include <iostream>
#include "Polybius.h"
#include <fstream>

int main(int argc, char** argv){
    std::fstream in(argv[1], std::ios_base::in | std::ios_base::out | std::ios_base::app);
    std::fstream out(argv[2], std::ios_base::in | std::ios_base::out | std::ios_base::app);

    if(in && out) {
        char line[256];
        while (!in.eof()) {
            in.getline(line, 256);
            if (argv[3][0] == '1')
                out << PolybiusCrypt(line) << std::endl;
            if (argv[3][0] == '0')
                out << PolybiusDecrypt(line) << std::endl;
        }
        in.close();
        out.close();
    }
    else
        std::cout << "failed to open file" << std::endl;

    return 0;
}