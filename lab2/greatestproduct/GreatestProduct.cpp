
#include "GreatestProduct.h"

#include <iostream>
#include <algorithm>

int GreatestProduct(const std::vector<int> &numbers, int k){

    std::vector<int> tmp = numbers;
    std::sort(tmp.begin(), tmp.end());

    int ret = 1;
    int i = 0, j = tmp.size()-1;
    if(k%2==1)
    {
        ret*=tmp[j];
        j-=1;
    }
    while(k>1)
    {
        if(ret*tmp[i]*tmp[i+1] > ret*tmp[j]*tmp[j-1])
        {
            ret *= (tmp[i]*tmp[i+1]);
            i+=2;
        }
        else
        {
            ret *= (tmp[j]*tmp[j-1]);
            j-=2;
        }
        k-=2;
    }
    return ret;
}