
#include "XorCypherBreaker.h"
#include <algorithm>
#include <iostream>

void IncrementKey(std::vector<int> &key)
{
    for (int i = key.size() - 1; i >= 0; --i) {
        if (key[i] == 122) {
            key[i] = 97;
            continue;
        }
        ++key[i];
        break;
    }
}

std::string XorCypherBreaker(const std::vector<char> &cryptogram, int key_length, const std::vector<std::string> &dictionary){
    std::string ret = "";

    std::vector<int> key(key_length, 97);

    std::vector<std::string> message;

    for(int found = 0;; IncrementKey(key)) {
        message.clear();
        std::string tmp = "";
        int k = 0;
        for (char ch : cryptogram) {
            if ((ch ^ key[k]) >= 'a' && (ch ^ key[k]) <= 'z')
                tmp += char(ch ^ key[k]);
            else if (((ch ^ 32) ^ key[k]) >= 'a' && ((ch ^ 32) ^ key[k]) <= 'z')
                tmp += char((ch ^ 32) ^ key[k]);
            else {
                if(tmp.length()>0)
                message.push_back(tmp);
                tmp = "";
            }
            ++k;
            if(k>=key_length)
                k = 0;
        }
        if(tmp.length()>0)
        message.push_back(tmp);
        tmp = "";
        found = 0;
        for (auto word : message) {
            std::cout << word << " ";
            if (std::find(dictionary.begin(), dictionary.end(), word) != dictionary.end())
                ++found;
        }
        std::cout << std::endl;
        if(found>message.size()/4)
            break;
    }
    for(int k : key)
        ret += char(k);

    return ret;
}