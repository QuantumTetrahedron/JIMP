//
// Created by borebart on 07.03.17.
//

#include "SimpleForwardList.h"

ForwardList* CreateNode(int value){
    ForwardList* f = new ForwardList;
    f->value = value;
    f->next = nullptr;
    return f;
}

void DestroyList(ForwardList* list) {
    ForwardList *current;
    while (list!=nullptr)
    {
        current = list;
        ForwardList *prev = nullptr;
        while (current->next != nullptr) {
            prev = current;
            current = current->next;
        }
        if(current == list){
            list = nullptr;
        }
        delete (current);
        if(prev != nullptr)
            prev->next = nullptr;
    }
}


ForwardList* PushFront(ForwardList* list, int value){
    if(list == nullptr)
        return nullptr;
    ForwardList* n = CreateNode(value);
    n->next = list;
    return n;
}

void Append(ForwardList* list, ForwardList* tail){
    if(list == nullptr)
    {
        list = tail;
        return;
    }
    ForwardList* i = list;
    while(i->next!=nullptr){
        i = i->next;
    }
    i->next = tail;
}
