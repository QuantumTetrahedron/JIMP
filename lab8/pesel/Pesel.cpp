//
// Created by borebart on 25.04.17.
//

#include "Pesel.h"

namespace academia{

    AcademiaDataValidationError::AcademiaDataValidationError() : std::runtime_error(""){}
    AcademiaDataValidationError::AcademiaDataValidationError(std::string message) : std::runtime_error(message) {}

    InvalidPeselChecksum::InvalidPeselChecksum(std::string pesel, int checksum)
            : AcademiaDataValidationError("Invalid PESEL("+pesel+") checksum: "+std::to_string(checksum)){
    }


    InvalidPeselLength::InvalidPeselLength(std::string pesel, int length)
            : AcademiaDataValidationError("Invalid PESEL("+pesel+") length: "+std::to_string(length)){
    }

    InvalidPeselCharacter::InvalidPeselCharacter(std::string pesel)
            : AcademiaDataValidationError("Invalid PESEL("+pesel+") character set") {
    }

    Pesel::Pesel(std::string p) {
        if(p.length()!=11)
            throw InvalidPeselLength(p,(int)p.length());

        int w[4] = {9,7,3,1};
        int sum = 0;
        for(int i=0;i<10;++i)
        {
            char ch = p[i];
            if(ch >= '0' && ch <= '9')
                sum += w[i%4]*((int)ch - (int)'0');
            else
                throw InvalidPeselCharacter(p);
        }
        if(sum%10 != p[10]-(int)'0')
            throw InvalidPeselChecksum(p,sum%10);
    }
}