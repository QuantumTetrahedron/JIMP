//
// Created by borebart on 25.04.17.
//

#ifndef JIMP_EXERCISES_PESEL_H
#define JIMP_EXERCISES_PESEL_H
#include <string>
#include <stdexcept>

namespace academia{
    class Pesel{
    public:
        Pesel(std::string p);
    private:
        std::string pesel;
    };

    class AcademiaDataValidationError : public std::runtime_error{
    public:
        AcademiaDataValidationError();
        AcademiaDataValidationError(std::string message);
        virtual ~AcademiaDataValidationError(){}
    };

    class InvalidPeselChecksum : public AcademiaDataValidationError{
    public:
        InvalidPeselChecksum(std::string pesel, int checksum);
    };

    class InvalidPeselLength : public AcademiaDataValidationError{
    public:
        InvalidPeselLength(std::string pesel, int length);
    };

    class InvalidPeselCharacter : public AcademiaDataValidationError{
    public:
        InvalidPeselCharacter(std::string pesel);
    };
}

#endif //JIMP_EXERCISES_PESEL_H

