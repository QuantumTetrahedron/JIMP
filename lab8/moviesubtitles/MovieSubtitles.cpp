
#include "MovieSubtitles.h"

#include<regex>

#include <iostream>

namespace moviesubs{

    void MicroDvdSubtitles::ShiftAllSubtitlesBy(int delay, int framerate, std::stringstream *in, std::stringstream *out) {
        if(framerate<0)
            throw std::invalid_argument("invalid framerate");
        std::string str;
        int line = 0;
        while (std::getline(*in, str)) {
            ++line;
            int actualDelay = delay * framerate / 1000;
            std::regex r{R"(\{([0-9]+)\}\{([0-9]+)\}(.*))"};
            std::smatch sm;
            if(std::regex_match(str, sm, r))
            {
                int t1, t2;
                std::string s1 = sm[1], s2 = sm[2];
                t1 = atoi(s1.c_str());
                t2 = atoi(s2.c_str());
                if(t2<t1)
                    throw SubtitleEndBeforeStart(line, str);
                if(t1+actualDelay<0||t2+actualDelay<0)
                    throw NegativeFrameAfterShift();

                *out << "{" << t1+actualDelay << "}{" << t2+actualDelay << "}" << sm[3] << std::endl;
            }
            else
            {
                throw InvalidSubtitleLineFormat();
            }
            str.clear();
        }
    }

    void SubRipSubtitles::ShiftAllSubtitlesBy(int delay, int framerate, std::stringstream *in, std::stringstream *out) {
        if(framerate<0)
            throw std::invalid_argument("invalid framerate");

        std::string str;
        int step = 1;
        std::regex step1reg{R"([0-9]+)"};
        std::regex step2reg{R"(([0-9]+):([0-9]{2}):([0-9]{2}),([0-9]{3}) --> ([0-9]{2}):([0-9]{2}):([0-9]{2}),([0-9]{3}))"};
        std::smatch sm;

        int line = 0;
        while(std::getline(*in, str)){

            if(step == 1){
                if(std::regex_match(str, step1reg))
                {
                    ++line;
                    if(std::to_string(line)!=str)
                        throw OutOfOrderFrames();
                    *out << str << std::endl;
                    step = 2;
                }
            }
            else if(step == 2) {
                if(std::regex_match(str, sm, step2reg))
                {
                    int h1, m1, h2, m2, f1, f2, s1, s2;
                    h1 = atoi(static_cast<std::string>(sm[1]).c_str());
                    m1 = atoi(static_cast<std::string>(sm[2]).c_str());
                    s1 = atoi(static_cast<std::string>(sm[3]).c_str());
                    f1 = atoi(static_cast<std::string>(sm[4]).c_str());
                    h2 = atoi(static_cast<std::string>(sm[5]).c_str());
                    m2 = atoi(static_cast<std::string>(sm[6]).c_str());
                    s2 = atoi(static_cast<std::string>(sm[7]).c_str());
                    f2 = atoi(static_cast<std::string>(sm[8]).c_str());

                    if(h1>h2||(h1==h2&&m1>m2)||(h1==h2&&m1==m2&&s1>s2))
                        throw SubtitleEndBeforeStart(line,str);

                    f1+=delay;
                    while(f1>=1000)
                    {
                        f1-=1000;
                        s1+=1;
                        if(s1>=60)
                        {
                            s1-=60;
                            m1+=1;
                            if(m1>=60)
                            {
                                m1-=60;
                                h1+=1;
                            }
                        }
                    }
                    while(f1<0)
                    {
                        f1+=1000;
                        s1-=1;
                        if(s1<0)
                        {
                            s1+=60;
                            m1-=1;
                            if(m1<0)
                            {
                                m1+=60;
                                h1-=1;
                                if(h1<0)
                                    throw NegativeFrameAfterShift();
                            }
                        }
                    }
                    f2+=delay;
                    while(f2>=1000)
                    {
                        f2-=1000;
                        s2+=1;
                        if(s2>=60)
                        {
                            s2-=60;
                            m2+=1;
                            if(m2>=60)
                            {
                                m2-=60;
                                h2+=1;
                            }
                        }
                    }
                    while(f2<0)
                    {
                        f2+=1000;
                        s2-=1;
                        if(s2<0)
                        {
                            s2+=60;
                            m2-=1;
                            if(m2<0)
                            {
                                m2+=60;
                                h2-=1;
                                if(h2<0)
                                    throw NegativeFrameAfterShift();
                            }
                        }
                    }
                    if(h1<10)
                        *out << "0";
                    *out << std::to_string(h1) << ":";
                    if(m1<10)
                        *out << "0";
                    *out << std::to_string(m1) << ":";
                    if(s1<10)
                        *out << "0";
                    *out << std::to_string(s1) << ",";
                    if(f1< 100)
                        *out << "0";
                    if(f1 < 10)
                        *out << "0";
                    *out << std::to_string(f1);

                    *out << " --> ";

                    if(h2<10)
                        *out << "0";
                    *out << std::to_string(h2) << ":";
                    if(m2<10)
                        *out << "0";
                    *out << std::to_string(m2) << ":";
                    if(s2<10)
                        *out << "0";
                    *out << std::to_string(s2) << ",";
                    if(f2< 100)
                        *out << "0";
                    if(f2 < 10)
                        *out << "0";
                    *out << std::to_string(f2);

                    *out << std::endl;

                    step = 3;
                }
                else
                    throw InvalidSubtitleLineFormat();
            }
            else if(step == 3)
            {
                *out << str;
                if(str.length()==0)
                {
                    step = 1;
                }
                *out<<std::endl;
            }

            str.clear();
        }
    }
}