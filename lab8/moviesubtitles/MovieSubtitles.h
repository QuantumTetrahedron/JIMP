
#ifndef JIMP_EXERCISES_MOVIESUBTITLES_H
#define JIMP_EXERCISES_MOVIESUBTITLES_H

#include <sstream>

namespace moviesubs{

    class MovieSubtitles{
    public:
        virtual void ShiftAllSubtitlesBy(int delay, int framerate, std::stringstream *in, std::stringstream *out) = 0;
    };

    class MicroDvdSubtitles : public MovieSubtitles{
    public:
        void ShiftAllSubtitlesBy(int delay, int framerate, std::stringstream *in, std::stringstream *out) override;
    };

    class SubRipSubtitles : public MovieSubtitles{
    public:
        void ShiftAllSubtitlesBy(int delay, int framerate, std::stringstream *in, std::stringstream *out) override;
    };

    class NegativeFrameAfterShift : public std::runtime_error{
    public:
        NegativeFrameAfterShift() : runtime_error("negative frame after shift"){}
    };

    class SubtitleEndBeforeStart : public std::runtime_error{
    public:
        SubtitleEndBeforeStart(int line, std::string message) : _line(line), runtime_error("At line "+std::to_string(line)+": "+message){}
        int LineAt() const {return _line;}
    private:
        int _line;
    };

    class InvalidSubtitleLineFormat : public std::runtime_error{
    public:
        InvalidSubtitleLineFormat() : runtime_error("invalid subtitle line format"){}
    };

    class MissingTimeSpecification : public std::runtime_error{
    public:
        MissingTimeSpecification() : runtime_error("missing time specification"){}
    };

    class OutOfOrderFrames : public std::runtime_error{
    public:
        OutOfOrderFrames():runtime_error("out of order frames"){}
    };
}

#endif //JIMP_EXERCISES_MOVIESUBTITLES_H
