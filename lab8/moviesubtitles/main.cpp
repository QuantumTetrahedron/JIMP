
#include "MovieSubtitles.h"

#include <iostream>

using namespace moviesubs;

int main(){

    MicroDvdSubtitles subs;
    std::stringstream in {"{0}{250}TEXT TEXT\n{260}{300}NEWLINE\n"};
    std::stringstream out;
    subs.ShiftAllSubtitlesBy(300,24,&in,&out);

    std::cout << out.str();

    return 0;
}