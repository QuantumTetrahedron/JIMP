//
// Created by borebart on 23.05.17.
//

#ifndef JIMP_EXERCISES_FACTORYMETHOD_H
#define JIMP_EXERCISES_FACTORYMETHOD_H

#include<vector>
#include <cstdlib>

namespace factoryMethod {
    template<class T>
    T Create() {
        return T();
    }

    class MyType {
    public:
        const char *SayHello() {
            return "hello";
        }
    };


    template<class T>
    T add(T a, T b) {
        return a + b;
    }

    template<class T>
    auto Value(T a) {
        return *a;
    }

    template<class R, class T>
    R Mean(std::vector<T> v){
        R sum = 0;
        for(T& x : v){
            sum += x;
        }
        return sum/v.size();
    }

    template<class T>
    class Repository{
    public:
        Repository():id(0){}

        size_t Size(){ return values.size(); }

        T operator[](int n){
                return values[n];
        };

        int NextId(){ return id++; }

        void add(T value){
            values.push_back(value);
        }

    private:
        int id;
        std::vector<T> values;
    };

    template<class T>
    class Logger{
    public:
        Logger(T& _out):out(_out){}

        template<class S>
        void Debug(S obj){ out << "Debug: " << obj; }

        template<class S>
        void Error(S obj){ out << "Error: "<< obj; }

        template<class S>
        void Warning(S obj){ out << "Warning: " <<obj; }

        template<class S>
        void Info(S obj){ out << "Info: "<< obj; }

    private:
        T& out;
    };
}


#endif //JIMP_EXERCISES_FACTORYMETHOD_H
