//
// Created by borebart on 23.05.17.
//

#include "FactoryMethod.h"
#include <iostream>
#include <sstream>
using namespace factoryMethod;

int main()
{
    int a=5;
    int* b = &a;

    int c = Value(b);

    std::vector<int> v = {1,3,3};
    std::cout << Value(std::begin(v)) << std::endl;
    std::cout << Mean<double>(v) <<std::endl;

    Repository<MyType> repo;
    repo.add(MyType());
    std::cout << repo.Size() << std::endl;
    std::cout << repo[0].SayHello() << std::endl;
    std::cout << repo.NextId() << std::endl;
    std::cout << repo.NextId() << std::endl;

    std::stringstream stream;
    Logger<std::stringstream> l(stream);
    l.Error("asdf");
    std::cout << stream.str() << std::endl;

    return c;
}