
#include "TextPool.h"

namespace pool{

    TextPool::TextPool() {}

    TextPool::TextPool(std::initializer_list<std::experimental::string_view> list) : pool(list){}

    TextPool::~TextPool() {}

    TextPool::TextPool(TextPool &&other) {
        pool = other.pool;
        other.pool.clear();
    }

    TextPool &TextPool::operator=(TextPool &&other) {
        if(this == &other)
            return other;
        pool = other.pool;
        other.pool.clear();
        return *this;
    }

    std::experimental::string_view TextPool::Intern(const std::string &str) {
        return *pool.emplace(str).first;
    }

    size_t TextPool::StoredStringCount() const {
        return pool.size();
    }
}