
#ifndef JIMP_EXERCISES_TEXTPOOL_H
#define JIMP_EXERCISES_TEXTPOOL_H

#include <initializer_list>
#include <string>
#include <experimental/string_view>
#include <set>

namespace pool{
    class TextPool{
    public:
        TextPool();
        TextPool(std::initializer_list<std::experimental::string_view> list);
        ~TextPool();
        TextPool(const TextPool& other) = delete;
        TextPool(TextPool &&other);
        TextPool& operator=(const TextPool& other) = delete;
        TextPool& operator=(TextPool &&other);
        std::experimental::string_view Intern(const std::string &str);
        size_t StoredStringCount() const;
    private:
        std::set<std::experimental::string_view> pool;
    };
}

#endif //JIMP_EXERCISES_TEXTPOOL_H
