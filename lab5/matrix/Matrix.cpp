
#include "Matrix.h"
#include <iostream>

namespace algebra{

    Matrix::Matrix(int size_x, int size_y) {
        if(!size_x && !size_y)
            mat = nullptr;
        else
            mat = new std::complex<double>*[size_x];
        for(int i=0;i<size_x;++i)
        {
            mat[i] = new std::complex<double>[size_y];
            for(int j=0;j<size_y;++j)
            {
                if(i==j)
                    mat[i][j] = 1;
                else
                    mat[i][j] = 0;
            }
        }
        size.first = size_x;
        size.second = size_y;
    }

    Matrix::Matrix(const char *str) {
        *this = Matrix(static_cast<std::string>(str));
    }

    Matrix::Matrix(std::string str) {
        int lastX = 0;
        int x_count = 0;
        int y_count = 0;
        bool next = true;
        for(char c : str)
        {
            if(next && (c < '0' || c > '9') && c!='[' && c!='i' && c!= '.')
            {
                x_count++;
                next = false;
            }
            if(c >= '0' && c <='9')
                next = true;
            if(c == ';' || c==']')
            {
                y_count++;
                if(lastX && lastX!=x_count)
                    throw std::runtime_error("Wrong matrix format");
                lastX = x_count;
                x_count = 0;
            }
        }
        x_count = lastX;
        mat = new std::complex<double>*[y_count];
        for(int i=0;i<y_count;++i)
        {
            mat[i] = new std::complex<double>[x_count];
        }
        size.first = y_count;
        size.second = x_count;
        std::string re_tmp;
        std::string im_tmp;
        int i=0,j=0;
        bool real = true;
        bool add;
        bool added = false;
        for(char c : str)
        {
            add = false;
            if((c >= '0' && c <='9') || c == '.')
            {
                added = false;
                if(real)
                    re_tmp+=c;
                else
                    im_tmp+=c;
            }
            if(c == 'i')
            {
                real = false;
            }
            if((c==' ' || c==';' || c==']') && !added)
            {
                add = true;
                real = true;
            }
            if(add)
            {
                added = true;
                if(im_tmp=="")
                    im_tmp="0";
                mat[i][j] = std::complex<double>(atof(re_tmp.c_str()), atof(im_tmp.c_str()));
                re_tmp = "";
                im_tmp = "";

                j++;
            }
            if(c==';' || c==']')
            {
                j=0;
                i++;
            }
        }
    }

    Matrix::Matrix(std::initializer_list<std::vector<std::complex<double>>> list) {
        std::vector<std::vector<std::complex<double>>> m = list;

        int lastY = 0;
        for(int i=0;i<m.size();++i)
        {
            if(lastY && lastY != m[i].size())
                throw std::runtime_error("Failed to create matrix: wrong matrix format");
            lastY = m[i].size();
        }
        size.first = m.size();
        size.second = lastY;

        mat = new std::complex<double>*[m.size()];
        for(int i=0;i<size.first;++i)
        {
            mat[i] = new std::complex<double>[size.second];
            for(int j=0;j<size.second;++j)
            {
                mat[i][j] = m[i][j];
            }
        }
    }

    Matrix::~Matrix() {
        if(mat!=nullptr) {
            for (int i = 0; i < size.first; ++i)
                delete[] mat[i];
            delete[] mat;
        }
    }

    Matrix::Matrix(const Matrix &other) {
        if(other.size.first == 0)
        {
            *this = Matrix();
            return;
        }
        size = other.size;
        mat = new std::complex<double>*[size.first];
        for(int i=0;i<size.first;++i)
        {
            mat[i] = new std::complex<double>[size.second];
            for(int j=0;j<size.second;++j)
            {
                mat[i][j] = other.mat[i][j];
            }
        }
    }

    Matrix::Matrix(Matrix &&other) {
        if(other.size.first == 0)
        {
            *this = Matrix();
            return;
        }
        size = other.size;
        mat = new std::complex<double>*[size.first];
        for(int i=0;i<size.first;++i)
        {
            mat[i] = new std::complex<double>[size.second];
            for(int j=0;j<size.second;++j)
            {
                mat[i][j] = other.mat[i][j];
            }
        }
        other.size.first=0;
        other.size.second=0;

        for(int i=0;i<size.first;++i)
            delete[] other.mat[i];
        delete[] other.mat;
        other.mat = nullptr;
    }

    Matrix &Matrix::operator=(const Matrix &other) {
        if (this == &other) {
            return *this;
        }

        if(size.first) {
            for (int i = 0; i < size.first; ++i)
                delete[] mat[i];
            delete[] mat;
        }

        if(!other.size.first)
        {
            size = other.size;
            mat = nullptr;
            return *this;
        }

        size = other.size;
        mat = new std::complex<double>*[size.first];
        for(int i=0;i<size.first;++i)
        {
            mat[i] = new std::complex<double>[size.second];
            for(int j=0;j<size.second;++j)
            {
                mat[i][j] = other.mat[i][j];
            }
        }

        return *this;
    }

    Matrix &Matrix::operator=(Matrix &&other) {
        if (this == &other) {
            return other;
        }

        if(size.first) {
            for (int i = 0; i < size.first; ++i)
                delete[] mat[i];
            delete[] mat;
        }
        if(!other.size.first)
        {
            size = other.size;
            mat = nullptr;
            return *this;
        }
        size = other.size;
        mat = new std::complex<double>*[size.first];
        for(int i=0;i<size.first;++i)
        {
            mat[i] = new std::complex<double>[size.second];
            for(int j=0;j<size.second;++j)
            {
                mat[i][j] = other.mat[i][j];
            }
        }
        for(int i=0;i<size.first;++i)
            delete[] other.mat[i];
        delete[] other.mat;
        other.mat = nullptr;
        return *this;
    }

    std::string Matrix::print() const {
        if(!size.first || !size.second) {
            return "[]";
        }

        std::string ret = "[";
        for(int i=0;i<size.first;++i) {
            for (int j = 0; j < size.second; ++j)
                ret += toString(mat[i][j].real()) + "i" + toString(mat[i][j].imag()) + ", ";
            ret.pop_back();
            ret.pop_back();
            ret+="; ";
        }
        ret.pop_back();
        ret.pop_back();
        ret+=']';
        return ret;
    }

    Matrix Matrix::add(const Matrix &other) const {
        if(size.first != other.size.first || size.second != other.size.second)
            return Matrix();

        Matrix result{size.first, size.second};
        for(int i=0;i<size.first;++i)
            for(int j=0;j<size.second;++j)
                result.mat[i][j] = this->mat[i][j] + other.mat[i][j];
        return result;
    }

    Matrix Matrix::sub(const Matrix &other) const {
        if(size.first != other.size.first || size.second != other.size.second)
            return Matrix();
        Matrix result{size.first, size.second};
        for(int i=0;i<size.first;++i)
            for(int j=0;j<size.second;++j)
                result.mat[i][j] = this->mat[i][j] - other.mat[i][j];
        return result;
    }

    Matrix Matrix::mul(const Matrix &other) const {
        if(size.second != other.size.first)
            return Matrix();
        Matrix result(size.first, other.size.second);

        for(int i=0;i<size.first;++i)
        {
            for(int j=0;j<other.size.second;++j)
            {
                std::complex<double> r = 0+0i;
                for(int k=0;k<size.second;++k)
                {
                    r += this->mat[i][k] * other.mat[k][j];
                }
                result.mat[i][j] = r;
            }
        }

        return result;
    }

    Matrix Matrix::mul(double c) const {
        Matrix result(size.first, size.second);

        for(int i=0;i<size.first;++i)
        {
            for(int j=0;j<size.second;++j)
            {
                result.mat[i][j] = c*this->mat[i][j];
            }
        }

        return result;
    }

    Matrix Matrix::div(double c) const {
        return mul(1/c);
    }

    Matrix Matrix::pow(int n) const {
        if(size.first != size.second)
            return Matrix();
        Matrix result(size.first, size.second);

        for(int i=0;i<n;++i)
            result = result.mul(*this);

        return result;
    }

    Matrix::Matrix() {
        mat = nullptr;
        size.first = 0;
        size.second = 0;
    }

    std::string toString(double x) {
        std::string ret = std::to_string(x);
        if(ret.find('.') != ret.npos)
        {
            while(ret[ret.length()-1]=='0')
                ret.pop_back();
        }
        if(ret[ret.length()-1]=='.')
            ret.pop_back();
        return ret;
    }
}