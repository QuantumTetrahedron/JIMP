
#ifndef JIMP_EXERCISES_MATRIX_H
#define JIMP_EXERCISES_MATRIX_H

#include <complex>
#include <initializer_list>
#include <vector>

namespace algebra{
    class Matrix{
    public:
        Matrix();
        Matrix(int size_x, int size_y);
        Matrix(const char* str);
        Matrix(std::string str);
        Matrix(std::initializer_list<std::vector<std::complex<double>>> list);
        ~Matrix();
        Matrix(const Matrix &other);
        Matrix(Matrix &&other);
        Matrix& operator=(const Matrix &other);
        Matrix& operator=(Matrix &&other);

        std::string print() const;
        inline std::string Print() const { return print(); }
        inline std::pair<size_t, size_t> Size() const { return size; }
        Matrix add(const Matrix& other) const;
        Matrix sub(const Matrix& other) const;
        Matrix mul(const Matrix& other) const;
        Matrix mul(double c) const;
        Matrix div(double c) const;
        Matrix pow(int n) const;
        inline Matrix Add(const Matrix& other) const { return add(other); }
        inline Matrix Sub(const Matrix& other) const { return sub(other); }
        inline Matrix Mul(const Matrix& other) const { return mul(other); }
        inline Matrix Mul(double c) const { return mul(c); }
        inline Matrix Div(double c) const { return div(c); }
        inline Matrix Pow(int n) const {return pow(n); }
    private:
        std::complex<double> **mat;
        std::pair<size_t, size_t> size;
    };

    std::string toString(double x);
}

#endif //JIMP_EXERCISES_MATRIX_H
