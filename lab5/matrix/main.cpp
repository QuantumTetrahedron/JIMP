
#include "Matrix.h"
#include <iostream>

using namespace algebra;

int main(){
    Matrix m1("[1, 2, 3.04i6;1, 2, 3]");
    Matrix m2{{7 + 1.0i, 0., 0.}, {0., 1.0i, 8.}};
    Matrix m3("[2; 4; 9]");
    Matrix m4("[2, 1; 3, 2]");
    std::cout << m1.print() << std::endl;
    std::cout << m2.print() << std::endl;
    std::cout << m3.print() << std::endl;
    std::cout << m1.add(m2).print() << std::endl;
    std::cout << m1.sub(m2).print() << std::endl;
    std::cout << m1.mul(m3).print() << std::endl;
    std::cout << m1.mul(2).print() << std::endl;
    std::cout << m1.div(2).print() << std::endl;
    std::cout << m4.pow(5).print() << std::endl;
    return 0;
}