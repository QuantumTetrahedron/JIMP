//
// Created by borebart on 28.03.17.
//

#ifndef JIMP_EXERCISES_MEMORYCHUNK_H
#define JIMP_EXERCISES_MEMORYCHUNK_H

#include <cstdint>
#include <algorithm>

namespace memorychunk{
    class MemoryChunk{
    public:
        MemoryChunk();
        MemoryChunk(size_t sz);
        MemoryChunk(const MemoryChunk &other);
        MemoryChunk(MemoryChunk &&other);
        MemoryChunk &operator=(const MemoryChunk &other);
        MemoryChunk &operator=(MemoryChunk &&other);
        ~MemoryChunk();
        int8_t *MemoryAt(size_t offset) const;
        size_t ChunkSize() const;
    private:
        int8_t *chunk;
        size_t size;
    };
}

#endif //JIMP_EXERCISES_MEMORYCHUNK_H
