//
// Created by borebart on 28.03.17.
//

#include "MemoryChunk.h"

namespace memorychunk{

    MemoryChunk::MemoryChunk() {
        chunk = nullptr;
        size = 0;
    }

    MemoryChunk::MemoryChunk(size_t sz) {
        chunk = new int8_t[sz];
        size = sz;
    }

    MemoryChunk::MemoryChunk(const MemoryChunk &other) {
        chunk = new int8_t[other.size];
        std::copy(other.chunk, other.chunk+other.size, chunk);
        size = other.size;
    }

    MemoryChunk::MemoryChunk(MemoryChunk &&other) {
        chunk = nullptr;
        std::swap(chunk, other.chunk);
        size = other.size;
        other.size = 0;
    }

    MemoryChunk &MemoryChunk::operator=(const MemoryChunk &other) {
        if(this == &other)
            return *this;

        delete(chunk);
        chunk = new int8_t[other.size];
        size = other.size;
        std::copy(other.chunk, other.chunk+other.size, chunk);

        return *this;
    }

    MemoryChunk &MemoryChunk::operator=(MemoryChunk &&other) {
        if(this == &other)
            return other;

        delete(chunk);
        chunk = nullptr;
        std::swap(chunk,other.chunk);
        size = other.size;
        other.size = 0;

        return *this;
    }

    int8_t *MemoryChunk::MemoryAt(size_t offset) const {
        return chunk+offset;
    }

    size_t MemoryChunk::ChunkSize() const {
        return size;
    }

    MemoryChunk::~MemoryChunk() {
        delete [] chunk;
    }
}