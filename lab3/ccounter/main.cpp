//
// Created by borebart on 14.03.17.
//
#include "CCounter.h"
#include <iostream>

using namespace ccounter;

int main()
{
    std::unique_ptr<Counter> counter = Init();
    Inc("qwe", &counter);
    Inc("asd", &counter);
    SetCountsTo("qwe", 8, &counter);
    Inc("qwe", &counter);
    Inc("asd", &counter);
    
    std::cout << Counts(counter, "qwe") << std::endl << Counts(counter, "asd") << std::endl;
    
    return 0;
}