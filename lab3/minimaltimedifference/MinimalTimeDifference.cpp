//
// Created by borebart on 14.03.17.
//

#include "MinimalTimeDifference.h"

#include <regex>
namespace minimaltimedifference {
    unsigned int ToMinutes(std::string time_HH_MM) {
        unsigned int ret = 0;
        std::regex pattern{R"((\d{1,2}):(\d{2}))"};
        std::smatch matches;
        if (std::regex_match(time_HH_MM, matches, pattern)) {
            int h = atoi(static_cast<std::string>(matches[1]).c_str());
            int m = atoi(static_cast<std::string>(matches[2]).c_str());
            ret = h * 60 + m;
        }

        return ret;
    }

    unsigned int MinimalTimeDifference(const std::vector<std::string> &times) {
        unsigned int minTime = 721;
        for (int i = 0; i < times.size(); ++i) {
            unsigned int t1 = ToMinutes(times[i]);
            for (int j = i + 1; j < times.size(); ++j) {
                unsigned int t2 = ToMinutes(times[j]);
                unsigned int difference = abs(t2 - t1);
                if (difference > 720) {
                    int tmp = difference - 720;
                    difference -= 2 * tmp;
                }

                if (difference < minTime)
                    minTime = difference;
            }
        }

        return minTime;
    }
}