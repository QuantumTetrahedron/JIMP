//
// Created by borebart on 14.03.17.
//

#ifndef JIMP_EXERCISES_MINIMALTIMEDIFFERENCE_H
#define JIMP_EXERCISES_MINIMALTIMEDIFFERENCE_H

#include <string>
#include <vector>

namespace minimaltimedifference {
    unsigned int ToMinutes(std::string time_HH_MM);

    unsigned int MinimalTimeDifference(const std::vector<std::string> &times);
}
#endif //JIMP_EXERCISES_MINIMALTIMEDIFFERENCE_H
