//
// Created by borebart on 14.03.17.
//
#include "MinimalTimeDifference.h"
#include <iostream>

int main(){

    std::vector<std::string> times = {"6:15", "11:03", "1:27"};
    std::cout << minimaltimedifference::MinimalTimeDifference(times);

    return 0;
}
