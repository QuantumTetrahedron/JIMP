
#include "SmartTree.h"

#include <sstream>
namespace datastructures{
    std::unique_ptr <SmartTree> CreateLeaf(int value){
        auto ret = std::make_unique<SmartTree>();
        ret->value = value;
        ret->left = nullptr;
        ret->right = nullptr;
        return ret;
    }

    std::unique_ptr <SmartTree> InsertLeftChild(std::unique_ptr<SmartTree> tree, std::unique_ptr<SmartTree> left_subtree){
        tree->left = std::move(left_subtree);
        return tree;
    }

    std::unique_ptr <SmartTree> InsertRightChild(std::unique_ptr<SmartTree> tree, std::unique_ptr<SmartTree> right_subtree){
        tree->right = std::move(right_subtree);
        return tree;
    }

    void PrintTreeInOrder(const std::unique_ptr<SmartTree> &unique_ptr, std::ostream *out){
        if(unique_ptr->left)
            PrintTreeInOrder(unique_ptr->left, out);
        *out << unique_ptr->value << ", ";
        if(unique_ptr->right)
            PrintTreeInOrder(unique_ptr->right, out);
    }

    std::string DumpTree(const std::unique_ptr<SmartTree> &tree){
        if(tree == nullptr)
            return "[none]";
        std::stringstream ss;
        ss << tree->value;
        std::string ret = "[";
        ret+=ss.str();
        ret+=" ";
        ret+=DumpTree(tree->left);
        ret+=" ";
        ret+=DumpTree(tree->right);
        ret+="]";
        return ret;
    }

    std::unique_ptr <SmartTree> RestoreTree(const std::string &tree){
        std::stringstream ss;
        std::string v = "";
        int i=1;
        for(;tree[i] != '[' && tree[i] != ']';++i)
            v+=tree[i];
        if(v == "none")
            return nullptr;
        ss << v;
        int value;
        ss >> value;
        auto root = CreateLeaf(value);
        std::string leftSubtree = "";
        std::string rightSubtree = "";
        while(tree[i]!='[' && i<tree.length())
            ++i;

        if(i==tree.length())
            return root;
        int braceCount = 0;
        for(; i < tree.length();++i)
        {
            if(tree[i] == '[')
                ++braceCount;
            else if(tree[i] == ']')
                --braceCount;
            leftSubtree += tree[i];
            if(braceCount==0)
                break;
        }
        if(i==tree.length())
            return root;

        root->left = RestoreTree(leftSubtree);

        while(tree[i]!='[' && i<tree.length())
            ++i;
        if(i==tree.length())
            return root;
        braceCount = 0;
        for(; i < tree.length();++i)
        {
            if(tree[i] == '[')
                ++braceCount;
            else if(tree[i] == ']')
                --braceCount;
            rightSubtree += tree[i];
            if(braceCount==0)
                break;
        }
        if(i==tree.length())
            return root;
        root->right = RestoreTree(rightSubtree);
        return root;
    }
}