
#include "TinyUrl.h"
namespace tinyurl {
    std::unique_ptr<TinyUrlCodec> Init() {
        auto ret = std::make_unique<TinyUrlCodec>();
        ret->state = {'0', '0', '0', '0', '0', '0'};
        return ret;
    }

    void NextHash(std::array<char, 6> *state) {
        for (int i = 5; i >= 0; --i) {
            if ((*state)[i] == 'z') {
                (*state)[i] = '0';
                continue;
            } else if ((*state)[i] == 'Z')
                (*state)[i] = 'a';
            else if ((*state)[i] == '9')
                (*state)[i] = 'A';
            else
                (*state)[i]++;
            break;
        }
    }

    std::string Encode(const std::string &url, std::unique_ptr<TinyUrlCodec> *codec) {
        std::array<char, 6> hash = (*codec)->state;
        (*codec)->m[hash] = url;
        std::string ret = "";
        for(int i=0;i<6;++i)
            ret+=hash[i];
        NextHash(&((*codec)->state));
        return ret;
    }

    std::string Decode(const std::unique_ptr<TinyUrlCodec> &codec, const std::string &hash) {
        if(hash.length()!=6)
            return nullptr;
        std::array<char, 6> h;
        for(int i =0;i<6;++i)
            h[i] = hash[i];
        return codec->m.at(h);
    }
}