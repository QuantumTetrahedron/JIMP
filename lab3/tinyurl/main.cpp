#include "TinyUrl.h"
#include <iostream>

int main(){
    std::unique_ptr<tinyurl::TinyUrlCodec> p = tinyurl::Init();
    std::string hash = tinyurl::Encode("http://ai.ia.agh.edu.pl/wiki/pl:dydaktyka:jimp2:2017:labs:pamiec2", &p );
    std::cout << hash;
    std::cout<<std::endl<<tinyurl::Decode(p, hash) << std::endl;
    return 0;
}
